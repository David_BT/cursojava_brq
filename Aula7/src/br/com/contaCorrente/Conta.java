package br.com.contaCorrente;

import java.math.BigDecimal;
import java.util.List;

public abstract class Conta {

	private Integer numeroConta;
	private Integer numeroDigito;

	public List lancamentos;
	
	public abstract BigDecimal getTaxaRendimento();
	
	public abstract BigDecimal getIR();
	
	public abstract Boolean validarLancamento(Lancamento lancamento);
	
	public Boolean realizarLancamentos() {
		return Boolean.FALSE;
	}
	
	public BigDecimal getSaldo() {
		return new BigDecimal("6,99");
	}
}
