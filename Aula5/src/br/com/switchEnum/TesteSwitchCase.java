package br.com.switchEnum;

public class TesteSwitchCase {

	private static Integer getEnumValue(SwitchEnum enumeravel) {
		Integer retorno = null;
		
		switch (enumeravel) {
			case CASE_1:
				retorno = enumeravel.getValor() * 40;
				break;
			case CASE_2:
				retorno = enumeravel.getValor() * 30;
				break;
			case CASE_3:
				retorno = enumeravel.getValor() * 20;
				break;
			case CASE_4:
				retorno = enumeravel.getValor() * 10;
				break;
			default:
				retorno = enumeravel.getValor() * 1;
				break;
		}
		
		return retorno;
	}
	
	private static Integer getIntegerValue(Integer inteiro) {
		Integer retorno = null;
		
		switch (inteiro) {
			case 10:
				retorno = 40;
				break;
			case 20:
				retorno = 30;
				break;
			case 30:
				retorno = 20;
				break;
			case 40:
				retorno = 10;
				break;
			default:
				retorno = 1;
				break;
		}
		
		return retorno;
	}
	
	private static String getStringValue(String cadeiaDeCaracter) {
		String retorno = null;
		
		switch (cadeiaDeCaracter) {
			case "String 1":
				retorno = "Achou 1";
				break;
			case "String 2":
				retorno = "Achou 2";
				break;
			case "String 3":
				retorno = "Achou 3";
				break;
			case "String 4":
				retorno = "Achou 4";
				break;
			default:
				retorno = "N�o Achou";
				break;
		}
		
		return retorno;
	}

	
	public static void main(String[] args) {
		
		System.out.println("GetEnumValue");
		System.out.println(getEnumValue(SwitchEnum.CASE_1));
		System.out.println(getEnumValue(SwitchEnum.CASE_2));
		System.out.println(getEnumValue(SwitchEnum.CASE_3));
		System.out.println(getEnumValue(SwitchEnum.CASE_4));
		System.out.println(getEnumValue(SwitchEnum.CASE_5));
		
		System.out.println("GetIntegerValue");
		System.out.println(getIntegerValue(10));
		System.out.println(getIntegerValue(20));
		System.out.println(getIntegerValue(30));
		System.out.println(getIntegerValue(40));
		System.out.println(getIntegerValue(50));
		
		System.out.println("GetStringValue");
		System.out.println(getStringValue("String 1"));
		System.out.println(getStringValue("String 2"));
		System.out.println(getStringValue("String 3"));
		System.out.println(getStringValue("String 4"));
		System.out.println(getStringValue("String 5"));

		
	}
}
