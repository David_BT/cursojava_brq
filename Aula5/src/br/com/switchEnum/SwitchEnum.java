package br.com.switchEnum;

public enum SwitchEnum {

	CASE_1(1),
	CASE_2(2),
	CASE_3(3),
	CASE_4(4),
	CASE_5(5);
	
	SwitchEnum(Integer valor){
		this.valor = valor;
	}
	
	private Integer valor;
	
	public Integer getValor() {
		return valor;
	}
	
}
