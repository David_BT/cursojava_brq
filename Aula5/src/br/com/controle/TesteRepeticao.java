package br.com.controle;

public class TesteRepeticao {

	
	public static void main(String[] args) {
		
		System.out.println("For");
		Long resultado = 0l;
		for (int i = 0; i < 100; i++) {
			resultado += i*i;
		}
		System.out.println(resultado +"\n");
		
		System.out.println("Foreach");
		String[] arrayString = {"A","B","C","D","E","F","G","H","I","J"};
		for (String atual : arrayString) {
			System.out.println(atual);
		}
		
		System.out.println("\n" +"While");
		int contador = 0;
		boolean variavelVerificacao = true;
		while (variavelVerificacao) {
			if (contador++ == 200) {
				variavelVerificacao = false;
			}
		}
		System.out.println(contador);

		
		System.out.println("\n" +"WhileDo");
		contador = 0;
		boolean variavelVerificacaoDoWhile = false;
		do {
			contador++;
		} while (variavelVerificacaoDoWhile);
		System.out.println(contador);

	}
}
