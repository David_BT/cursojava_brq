package br.com.colecoes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TesteColecoes {

	public static void main(String[] args) {
//		List listaObjetos = new ArrayList();
//		
//		listaObjetos.add("String 1");
//		listaObjetos.add("        String 2      6   ");
//		
//		for (Object currentObject : listaObjetos) {
//			String s = (String) currentObject;
//			
//			System.out.println(s.trim());
//			
//		}
		
		
		System.out.println("Teste Lista");

		List<String> listaObjetos = new ArrayList<>();
		listaObjetos.add("abobrinha");
		listaObjetos.add("ma�a");
		listaObjetos.add("abobora");
		listaObjetos.add("melancia");
		
		for (Object currentObject : listaObjetos) {
			String s = (String) currentObject;
		
			System.out.println(s.trim());
		}
		
		System.out.println("Teste Set");
		Set<Integer> conjuntoInteiros = new HashSet<Integer>();
		conjuntoInteiros.add(1);
		conjuntoInteiros.add(10);
		conjuntoInteiros.add(8);
		conjuntoInteiros.add(7);
		conjuntoInteiros.add(9);
		conjuntoInteiros.add(8);
		
		for (Integer inteiro : conjuntoInteiros) {
			System.out.println(inteiro);
		}
		
	}
	
}
