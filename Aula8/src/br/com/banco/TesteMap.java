package br.com.banco;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

public class TesteMap {

	public static void main(String[] args) {
		
		System.out.println(" HashMap ");

		Map<String, Integer> mapaIdades = new HashMap<>();
		
		mapaIdades.put("Davi", 26);
		mapaIdades.put("David", 28);
		mapaIdades.put("Hendrel", 22);
		mapaIdades.put("Peterson", 33);
		mapaIdades.put("Rodrigo", 35);
		
		String[] nomes = {"Davi", "David", "Angelo", "Bruno", "Rodrigo"};
		
		for (String nomeAtual : nomes) {
			if (mapaIdades.containsKey(nomeAtual)){
				System.out.println(String.format("A idade do %s � %d ", nomeAtual, mapaIdades.get(nomeAtual)));
			} else {
				System.out.println("N�o conhe�o o " +nomeAtual);
			}
		}
		
		
		System.out.println(" ");
		System.out.println(" SortedMap ");

		SortedMap<String, Integer> mapaIdadesOrdenado = new TreeMap<>();
		
		mapaIdadesOrdenado.put("Davi", 26);
		mapaIdadesOrdenado.put("David", 28);
		mapaIdadesOrdenado.put("Peterson", 33);
		mapaIdadesOrdenado.put("Rodrigo", 35);
		mapaIdadesOrdenado.put("Hendrel", 22);
		
				
		for (Entry<String, Integer> currentEntry: mapaIdadesOrdenado.entrySet()) {
			System.out.println(String.format("A idade do %s � %d ", currentEntry.getKey(), currentEntry.getValue()));
		}
	}
}
