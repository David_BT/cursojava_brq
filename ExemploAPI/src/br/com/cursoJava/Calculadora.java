package br.com.cursoJava;

public interface Calculadora {

	public Integer multiply(Integer...numeros);
	
	public Integer add(Integer...numeros);
	
	public Integer minus(Integer...numeros);
	
	public Integer divide(Integer...numeros);
}
