package br.com.cursoJava;

public interface CalculadoraComplexa {

	public Integer pow(Integer base, Integer...numeros);
	
	public Integer factory(Integer base);
	
	public Integer fib(Integer pos);
	
}
