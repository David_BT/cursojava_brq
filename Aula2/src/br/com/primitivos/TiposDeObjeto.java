package br.com.primitivos;

public class TiposDeObjeto {

	/**
	 * Funciona
	 * @param args
	 */
	public static void main(String[] args) {
		
		Integer wrapperInteiro = 2;
		System.out.println("Integer  " +wrapperInteiro);
		
		Character wrapperCaracter = 'u';
		System.out.println("Character " +wrapperCaracter);
		
		Boolean wrapperBooleano = true;
		System.out.println("Boolean " +wrapperBooleano);
		
		Long wrapperLong = 100l;
		System.out.println("Long " +wrapperLong);
		
		//Tipos de Objetos tem m�todos
		System.out.println(wrapperInteiro.getClass());

	}
	
}
