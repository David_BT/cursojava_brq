package br.com.primitivos;

public class Primitivos {
	
	public static void main(String[] args) {
		int primitivoInteiro = 5;
		System.out.println("Inteiro " +primitivoInteiro);
		
		primitivoInteiro = 10;
		System.out.println("Inteiro " +primitivoInteiro);

//		n�o � possivel usar uma vari�vel sem inicializar um valor
//		int primitivoInteiroSemValor;
//		System.out.println(primitivoInteiroSemValor);
		
		char primitivoCaractere = 'a';
		long primitivoLong = 112;
		boolean primitivoBoolean = false;
		
		System.out.println("Char " +primitivoCaractere);
		System.out.println("Long " +primitivoLong);
		System.out.println("Boolean " +primitivoBoolean);
		
	}
	
}
