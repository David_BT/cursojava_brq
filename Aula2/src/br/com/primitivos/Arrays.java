package br.com.primitivos;


public class Arrays {

	public static void main(String[] args) {
		
		String[] arrayDeStrings = new String[10];
		arrayDeStrings[0] = "Aula 1";
		arrayDeStrings[1] = "Aula 2";
		
		System.out.println(arrayDeStrings[0]);
		System.out.println(arrayDeStrings[1]);
		System.out.println(arrayDeStrings);
		
		
		//Operadores de igual (==) e diferente (!=) tamb�m s�o aplic�veis a objetos, por�m comparam suas refer�ncias e n�o valores
		
	}	
}
