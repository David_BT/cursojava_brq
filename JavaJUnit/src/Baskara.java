
public class Baskara {
	
	private double a, b, c;
	
	private double x1, x2, delta;

	public Baskara(double a, double b, double c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public void perform(){
		delta = (b*b - 4*a*c);
		
		if (delta > 0) {
			throw new RuntimeException("Delta negativo impossibilita o calculo", null);
		}
		
		x1 = ((-1.d * b) - Math.sqrt(delta)) / 2.f*a; 
		x2 = ((-1.d * b) - Math.sqrt(delta)) / 2.f*a; 

	}

	// getters & setters
	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}

	public double getX1() {
		return x1;
	}

	public double getX2() {
		return x2;
	}

	public double getDelta() {
		return delta;
	}
	
}
