import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BaskaraTest {

	public Baskara baskara;
	
	@Before
	public void setUp() throws Exception {
		baskara = new Baskara(0.d,  0.d, 0.d);
	}

	@After
	public void tearDown() throws Exception {
		baskara = null;
	}

	@Test
	public void testBaskaraEmpty(){
		assertTrue("Teste Valores Iniciais", 0.d == baskara.getA());
		assertTrue("Teste Valores Iniciais", 0.d == baskara.getB());
		assertTrue("Teste Valores Iniciais", 0.d == baskara.getC());		
	}
	
	@Test(expected = RuntimeException.class)
	public void testDeltaNegativo() {
		baskara = new Baskara(-2.d,  1.d, 4.d);
		baskara.perform();
	}
	
	@Test
	public void testBaskara() {
		fail("Not yet implemented");
	}

	@Test
	public void testPerform() {
		fail("Not yet implemented");
	}

}
