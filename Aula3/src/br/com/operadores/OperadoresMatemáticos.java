package br.com.operadores;

public class OperadoresMatemáticos {

	public static void main(String[] args) {
		
		long x, y, z;
		x = 0;
		y = 1;
		z = 2;
		
		// +
		System.out.println("Soma: " +(x + y));
		
		// -
		System.out.println("Subtração: " +(x - y));
		
		// *
		System.out.println("Multiplicação: " +(x * y));
		
		// /
		System.out.println("Divisão: " +(x / y));
		
		// %
		System.out.println("Resto: " +(x % y));
		
		// ++
		System.out.println(z++);
		System.out.println(++z);
		
		// --
		System.out.println(z--);
		System.out.println(--z);
		
	}
}
