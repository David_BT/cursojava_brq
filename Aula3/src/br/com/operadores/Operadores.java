package br.com.operadores;

public class Operadores {

	public static void main(String[] args) {
		//Teste de Operadores de comparação
		boolean testeIgual = 2 == 3;
		System.out.println(testeIgual);
		boolean testeDiferente = 2 != 3;
		System.out.println(testeDiferente);
		boolean testeMenor = 2 < 3;
		System.out.println(testeMenor);
		boolean testeMenorIgual = 2 <= 2;
		System.out.println(testeMenorIgual);
		boolean testeMaior = 2 > 3;
		System.out.println(testeMaior);
		boolean testeMaiorIgual = 2 >= 2;
		System.out.println(testeMaiorIgual);
		
		
	}
}
