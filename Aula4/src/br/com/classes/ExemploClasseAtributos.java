package br.com.classes;

public class ExemploClasseAtributos {

	//modificador default
	String cadeiaDeCaracter;
	
	//modificador publico
	public int umNumero;
	
	//modificador private
	private long umNumeroGrande;
	
	public static void main(String[] args) {
		ExemploClasseAtributos exemploClasseAtributos = new ExemploClasseAtributos();
		exemploClasseAtributos.umNumeroGrande = 1111l;
	}
}
